const schedule = require('node-schedule');
const fs = require('fs');
const path = require('path');

let times = 0;

/**
 * 每30s打印一次日志
 */
function scheduleSecondLog() {

    var rule = new schedule.RecurrenceRule();
    rule.second = [0, 30];
    schedule.scheduleJob(rule, function () {
        times++;
        log(`${new Date()} -- 打印第${times}次！`);
    });
}

/**
 * 每小时统计打印次数 一天24小时  正点打印 0分0秒
 */
let lastTimes = 0;
function scheduleHourLog() {
    var rule = new schedule.RecurrenceRule();
    rule.minute = 0;
    schedule.scheduleJob(rule, function () {
        log(`==============================【 每小时打印 】===== 【 当前时间 】 ${new Date()} ====> 近一个小时内打印的次数: ${times - lastTimes} `);
        lastTimes = times;
    });
}

/**
 * 每天统计  每天8点为一天
 */
let lastTimes_d = 0;
function scheduleDayLog() {
    var rule = new schedule.RecurrenceRule();
    rule.hour = 8;
    schedule.scheduleJob(rule, function () {
        log(`******************************【 每天打印 】****** 【 当前时间 】 ${new Date()} ******》》》 近一天内打印的次数: ${times - lastTimes_d} `);
        lastTimes_d = times;
    });
}

function log(text){
    let _file = path.join(__dirname, "./times.log");

    try {
        fs.appendFile(_file,  text+'\n', err => {
            if (err) {
                log(1,err);
            } 
        });
    } catch (error) {
        log(2,error);
    }
}

function exec() {
    scheduleSecondLog();
    scheduleHourLog();
    scheduleDayLog();
}

module.exports = exec;