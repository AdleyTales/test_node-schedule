const Koa = require('koa');
const Router = require('koa-router');
const scheduleUtil = require('./scheduleUtil');

const app = new Koa();
const router = new Router();

router.get('/test', async ctx => {
    let n = Math.random();
    ctx.body = n + '';
});

// times
scheduleUtil();

app.use(router.routes());
app.listen(9090, () => {
    console.log(`server is running at port 9090 ...`);
});